Welcome to the GENTOO install script, please note you must be at your system for instructions
this script will perform a majority of the configurations for you however certain interaction
may be required on your part.

NOTE: THIS WILL RESULT IN DATA LOSS OF THE CURRENT DRIVE
Please press enter when your ready to continue. 

	* Auto partition two disc ZFS
	* Get gentoo-latest*.tar.bz2
	* Unpack gentoo-latest*tar.bz2
	* Create make.conf
	* Install minimal soft
	* Grub install
	* genkernel 3.17.8