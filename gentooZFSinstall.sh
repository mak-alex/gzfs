#!/bin/bash
clear;
echo "Welcome to the GENTOO install script, please note you must be at your system for instructions"
echo "this script will perform a majority of the configurations for you however certain interaction"
echo "may be required on your part."
echo
echo
echo "NOTE: THIS WILL RESULT IN DATA LOSS OF THE CURRENT DRIVE"
echo "Please press enter when your ready to continue." 
read
# Loop over the four drives we're using
for f in a b ; do
	## EDIT ME:
	## Make up some partitions. We do 64M for grub, and the rest for ZFS.
	sgdisk --zap-all /dev/sd${f}
	sgdisk \
		--new=1:2048:514047 --typecode=1:8300 --change-name=1:"Linux filesystem" \
		--largest-new=2:514047:579583 --typecode=2:EF02 --change-name=2:"grub" \
		--largest-new=3 --typecode=3:BF01 --change-name=3:"zfs" \
		/dev/sd${f}
done
for f in a b ; do
	mkfs.ext2 -m 1 /dev/sd${f}1
done

echo -ne "Enter name pool: "
read namePool
echo -ne "Enter method compression: "
read methodCompression
zpool create -f -o ashift=12 -o cachefile=/tmp/zpool.cache -O normalization=formD --compression=${methodCompression} -m none -R /mnt/gentoo ${namePool} /dev/sda3
zpool add ${namePool} mirror /dev/sdc3
echo -ne "Add cache SSD? (y|n): "
read cacheSSD
if [ ${cacheSSD} == "y" ]; then
	echo -ne "Input please your device cache (ex.: sdb1): "
	read ssdDev
	zpool add ${namePool} cache /dev/${ssdDev}
fi
# Create some empty containers for organization purposes, and make the dataset that will hold /
zfs create -p ${namePool}/gentoo
zfs create -o mountpoint=/ ${namePool}/gentoo/root
# Optional, but recommended datasets: /home
zfs create -o mountpoint=/home ${namePool}/gentoo/home
# Optional datasets: /usr/src, /usr/portage/{distfiles,packages}
zfs create -o mountpoint=/usr/src ${namePool}/gentoo/src
zfs create -o mountpoint=/usr/portage -o compression=off ${namePool}/gentoo/portage
zfs create -o mountpoint=/usr/portage/distfiles ${namePool}/gentoo/portage/distfiles
zfs create -o mountpoint=/usr/portage/packages ${namePool}/gentoo/portage/packages

# Go into the directory that you will chroot into
cd /mnt/gentoo
wget http://mirror.neolabs.kz/gentoo/pub/releases/amd64/current-iso/stage3-amd64-20141204.tar.bz2
tar xjpf stage3-amd64-20141204.tar.bz2
# Make a boot folder and mount your boot drive
mkdir boot && mount /dev/sda1 boot

# Bind the kernel related directories
mount -t proc none proc
mount --rbind /dev dev
mount --rbind /sys sys

# Copy network settings
cp -f /etc/resolv.conf etc

# Make the zfs folder in 'etc' and copy your zpool.cache
mkdir etc/zfs
cp /tmp/zpool.cache etc/zfs
cat>>/mnt/gentoo/etc/portage/make.conf<<'EOF'
CFLAGS="-march=amdfam10 -O2 -pipe"
CXXFLAGS="${CFLAGS}"
SYS_CPU_TGT="6"
MAKEOPTS="--jobs --load=${SYS_CPU_TGT}"
EMERGE_DEFAULT_OPTS="--jobs --load-average=${SYS_CPU_TGT} --verbose --tree --keep-going --with-bdeps=y"
LINGUAS="en,ru"

SYS_USE_CPU="mmx sse sse2 sse3 ssse3 openmp opencl cuda posix nptl multilib smp lapack"
SYS_USE_LANG="perl python"
SYS_USE_TOOLKITS="-gtk2 -gtk3 -kde"
SYS_USE_GAPI="gd sdl ncurses xcb opengl v4l vdpau xv X dri"
SYS_USE_AAPI="openal alsa"
SYS_USE_OTHER="acl alsa cdr crypt cups dvd dvdr firefox gmp iconv nsplugin offensive pcre pda rss spell taglib truetype videos vim-syntax xattr xcomposite xft xinerama xml xscreensaver fontconfig qt3support phonon"
SYS_USE_COMPRESSION="bzip2 gzip lzma lzo szip zlib"

SYS_USE_MEDIA_GFX="imagemagick jpeg jpeg2k openexr png raw svg tiff wmf mng"
SYS_USE_MEDIA_AUDIO="aac cdda flac gsm lame mad mikmod shorten speex timidity vorbis mp3 midi"
SYS_USE_MEDIA_VIDEO="css dv ffmpeg theora x264 xvid"
SYS_USE_MEDIA_CONTAINERS="matroska mms mp4 mpeg ogg pdf quicktime vcd"
SYS_USE_MEDIA="${SYS_USE_MEDIA_GFX} ${SYS_USE_MEDIA_AUDIO} ${SYS_USE_MEDIA_VIDEO} ${SYS_USE_MEDIA_CONTAINERS} sound cddb encode exif gimp libsamplerate mtp ppds sndfile sox wavpack xmp latex"

SYS_USE_NET="avahi curl ftp geoip gnutls ipv6 libwww rdesktop samba sockets ssl tcpd vnc"
SYS_USE_PLATFORM="acpi dbus fam hddtemp ieee1394 joystick libnotify lm_sensors pam readline sharedmem syslog sysvipc threads udev unicode usb"
SYS_USE_DONOTWANT="-pulseaudio -gnome -oss -berkdb -gdbm"

USE="${SYS_USE_CPU} ${SYS_USE_LANG} ${SYS_USE_TOOLKITS} ${SYS_USE_GAPI} ${SYS_USE_AAPI} ${SYS_USE_OTHER} ${SYS_USE_MEDIA} ${SYS_USE_COMPRESSION} ${SYS_USE_NET} ${SYS_USE_PLATFORM} ${SYS_USE_DONOTWANT}"

GENTOO_MIRRORS="http://mirror.neolabs.kz/gentoo/pub/distfiles/ ftp://mirror.neolabs.kz/gentoo/pub/distfiles/ http://mirror.yandex.ru/gentoo-distfiles/distfiles/"
VIDEO_CARDS="nvidia"
INPUT_DEVICES="evdev"
ALSA_CARDS=""
ACCEPT_LICENSE="AdobeFlash-11.2"
ACCEPT_KEYWORDS="~amd64"
# PORTAGE_BINHOST="http://binhost.ossdl.de/x86_64-pc-linux-gnu/"
#PKGDIR="/mnt/r5/pkgdir"
#PORTAGE_TMPDIR="/mnt/r5/portage_tmp"
CHOST="x86_64-pc-linux-gnu"
EOF

cat>>/mnt/gentoo/doinstall.sh<<EOF
#!/bin/bash
emerge --sync;
emerge --load-average 10 --jobs 8 emerge grub vim awesome xorg-server virtualbox genkernel gentoo-sources --autounmask-write; dispatch-conf;
emerge --load-average 10 --jobs 8 emerge grub vim awesome xorg-server virtualbox genkernel gentoo-sources;
for dev in a b ; do
	grub-install /dev/sd${dev}
done
touch /boot/grub/grub.cfg
genkernel all --no-clean --no-mountboot --zfs --bootloader=grub2 --callback="emerge @module-rebuild";
emerge --load-average 10 --jobs 8 vixie-cron syslog-ng luarocks libreoffice-bin firefox-bin nvidia-drivers --autounmask-write;
emerge --load-average 10 --jobs 8 vixie-cron syslog-ng luarocks libreoffice-bin firefox-bin nvidia-drivers;
if [ $? == 0 ]; then 
	/opt/bin/nvidia-xconfig
	eselect opengl set 1
else
	echo "Dont work nvidia drivers"
fi
emerge --load-average 10 --jobs 8 zfs zfs-kmod spl dhcpcd --autounmask-write;
emerge --load-average 10 --jobs 8 zfs zfs-kmod spl dhcpcd;
if [ $? == 0 ]; then 
	rc-udpate add zfs boot;
else
	echo "ERROR ADD ZFS TO BOOT"
fi
# Comment the BOOT, ROOT and SWAP lines in /etc/fstab
sed -i -e "s/\(.*\)\/\(BOOT\|ROOT\|SWAP\)\(.*\)/\#\1\/\2\3/g" /etc/fstab

# Set portage niceness to minimize potential for updates to cause lag
echo PORTAGE_NICENESS=19 >> /etc/make.conf
rc-update add udev boot
# Add services as desired.
rc-update add vixie-cron default
## You might not want this if you're doing static IP...
rc-update add dhcpcd default
rc-update add sshd default

# New pass
echo "New pass for root: "
passwd root

# add new user
useradd -m -G users,wheel -s /bin/bash flashhacker
echo "New pass for user flashhacker: "
passwd flashhacker

# Make recovery snapshot after booting into new install
zfs snapshot ${namePool}/ROOT/gentoo@install

EOF
chmod +x /mnt/gentoo/doinstall.sh
# Chroot into Funtoo
env -i HOME=/root TERM=$TERM chroot /bin/bash /doinstall.sh

# Unmount all the kernel filesystem stuff and boot (if you have a separate /boot)
umount -l proc dev sys boot
cd / && zpool export ${namePool}